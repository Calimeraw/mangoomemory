package com.creusot.memorygame.controllers.screens;

import com.creusot.memorygame.screens.GameScreenFragment;
import com.creusot.memorygame.views.GameCardView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Cédric Creusot on 20/12/15
 */
@RunWith(MockitoJUnitRunner.class)
public class GameScreenControllerTest {

    private GameScreenFragment gameScreenFragment;
    private GameScreenController gameScreenController;

    @Before
    public void setUp() throws Exception {
        gameScreenFragment = mock(GameScreenFragment.class);
        gameScreenController = new GameScreenController(gameScreenFragment);
    }

    @Test
    public void testInitialize() throws Exception {
        // GIVEN

        // WHEN
        gameScreenController.initialize();

        // THEN
        assertThat(gameScreenController.getCountPair()).isEqualTo(2);
        verify(gameScreenFragment).setGridSizeGameGridLayout(2);
        verify(gameScreenFragment).setTime(anyLong());
    }

    @Test
    public void testGenerateGrid() throws Exception {
        // GIVEN
        gameScreenController.initialize();

        // WHEN
        int[][] grid = gameScreenController.generateGrid();

        // THEN
        assertThat(grid.length).isEqualTo(2);
        assertThat(grid[0].length).isEqualTo(2);
        assertThat(grid[0][0]).isNotEqualTo(0);
        assertThat(grid[0][1]).isNotEqualTo(0);
        assertThat(grid[1][0]).isNotEqualTo(0);
        assertThat(grid[1][1]).isNotEqualTo(0);
    }

    @Test
    public void testIsAllResourcesSet_false() throws Exception {
        // GIVEN
        int[] countPerResources = {0, 0};

        // WHEN
        boolean valueToTest = gameScreenController.isAllResourcesSet(countPerResources);

        // THEN
        assertThat(valueToTest).isFalse();
    }

    @Test
    public void testIsAllResourcesSet_true() throws Exception {
        // GIVEN
        int[] countPerResources = {2, 2};

        // WHEN
        boolean valueToTest = gameScreenController.isAllResourcesSet(countPerResources);

        // THEN
        assertThat(valueToTest).isTrue();
    }

    @Test
    public void testupdateGameState_NOTHING() throws Exception {
        // GIVEN
        GameCardView gameCardView1 = mock(GameCardView.class);
        GameCardView gameCardView2 = mock(GameCardView.class);
        gameScreenController.initialize();

        when(gameCardView1.getImageFrontResourceId()).thenReturn(0);
        when(gameCardView2.getImageFrontResourceId()).thenReturn(1);

        // WHEN
        int state = gameScreenController.updateGameState(null, null);

        // THEN
        assertThat(state).isEqualTo(GameScreenController.GAME_STATE_DO_NOTHING);
    }

    @Test
    public void testupdateGameState_NOTEQUALS() throws Exception {
        // GIVEN
        GameCardView gameCardView1 = mock(GameCardView.class);
        GameCardView gameCardView2 = mock(GameCardView.class);
        gameScreenController.initialize();

        when(gameCardView1.getImageFrontResourceId()).thenReturn(0);
        when(gameCardView2.getImageFrontResourceId()).thenReturn(1);

        // WHEN
        int state = gameScreenController.updateGameState(gameCardView1, gameCardView2);

        // THEN
        assertThat(state).isEqualTo(GameScreenController.GAME_STATE_PAIR_NOT_EQUAL);
    }

    @Test
    public void testupdateGameState_EQUALS() throws Exception {
        // GIVEN
        GameCardView gameCardView1 = mock(GameCardView.class);
        GameCardView gameCardView2 = mock(GameCardView.class);
        gameScreenController.initialize();

        when(gameCardView1.getImageFrontResourceId()).thenReturn(1);
        when(gameCardView2.getImageFrontResourceId()).thenReturn(1);

        // WHEN
        int state = gameScreenController.updateGameState(gameCardView1, gameCardView2);

        // THEN
        assertThat(state).isEqualTo(GameScreenController.GAME_STATE_PAIR_EQUAL);
    }

    @Test
    public void testupdateGameState_WIN() throws Exception {
        // GIVEN
        GameCardView gameCardView1 = mock(GameCardView.class);
        GameCardView gameCardView2 = mock(GameCardView.class);

        when(gameCardView1.getImageFrontResourceId()).thenReturn(1);
        when(gameCardView2.getImageFrontResourceId()).thenReturn(1);

        // WHEN
        int state = gameScreenController.updateGameState(gameCardView1, gameCardView2);

        // THEN
        assertThat(state).isEqualTo(GameScreenController.GAME_STATE_WIN);
    }
}
