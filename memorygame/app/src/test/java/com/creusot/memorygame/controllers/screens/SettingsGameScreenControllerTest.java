package com.creusot.memorygame.controllers.screens;

import com.creusot.memorygame.screens.SettingsGameScreenFragment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by Cédric Creusot on 20/12/15
 */
@RunWith(MockitoJUnitRunner.class)
public class SettingsGameScreenControllerTest {

    private SettingsGameScreenFragment settingsGameScreenFragment;
    private SettingsGameScreenController settingsGameScreenController;

    @Before
    public void setUp() throws Exception {
        settingsGameScreenFragment = mock(SettingsGameScreenFragment.class);
        settingsGameScreenController = new SettingsGameScreenController(settingsGameScreenFragment);
    }

    @Test
    public void testUpdateGridSize_initialize() throws Exception {
        // GIVEN

        // WHEN
        settingsGameScreenController.initialize();

        // THEN
        assertThat(settingsGameScreenController.getGridSize()).isEqualTo(2);
        verify(settingsGameScreenFragment).setSeekbarSizeMax(2);
        verify(settingsGameScreenFragment).setSizeIndicatorGridSizeText(2);
        verify(settingsGameScreenFragment).setGridPreviewSize(2, 2);
    }

    @Test
    public void testUpdateGridSize_value_0() throws Exception {
        // GIVEN

        // WHEN
        settingsGameScreenController.updateGridSize(0);

        // THEN
        assertThat(settingsGameScreenController.getGridSize()).isEqualTo(2);
        verify(settingsGameScreenFragment).setSizeIndicatorGridSizeText(anyInt());
        verify(settingsGameScreenFragment).setGridPreviewSize(anyInt(), anyInt());
    }


    @Test
    public void testUpdateGridSize_value_2() throws Exception {
        // GIVEN

        // WHEN
        settingsGameScreenController.updateGridSize(1);

        // THEN
        assertThat(settingsGameScreenController.getGridSize()).isEqualTo(4);
        verify(settingsGameScreenFragment).setSizeIndicatorGridSizeText(anyInt());
        verify(settingsGameScreenFragment).setGridPreviewSize(anyInt(), anyInt());
    }

    @Test
    public void testUpdateGridSize_value_3() throws Exception {
        // GIVEN

        // WHEN
        settingsGameScreenController.updateGridSize(2);

        // THEN
        assertThat(settingsGameScreenController.getGridSize()).isEqualTo(6);
        verify(settingsGameScreenFragment).setSizeIndicatorGridSizeText(anyInt());
        verify(settingsGameScreenFragment).setGridPreviewSize(anyInt(), anyInt());
    }
}
