package com.creusot.memorygame;

import android.app.Application;
import android.content.ContextWrapper;

import com.pixplicity.easyprefs.library.Prefs;

/**
 * Created by Cédric Creusot on 20/12/15
 */
public class GameApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        new Prefs.Builder().setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }
}
