package com.creusot.memorygame.screens;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.creusot.memorygame.R;
import com.creusot.memorygame.adapters.ScoreListAdapter;
import com.creusot.memorygame.screens.interfaces.BaseScreenFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Cédric Creusot on 19/12/15.
 */
public class ScoreScreenFragment extends Fragment implements BaseScreenFragment {

    @Bind(R.id.scoreListView)
    ListView scoreLisView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_score_screen, container, false);
        ButterKnife.bind(this, view);

        scoreLisView.setAdapter(new ScoreListAdapter(inflater, getActivity()));
        return view;
    }

    @Override
    public String getTransactionTag() {
        return ScoreScreenFragment.class.getName();
    }
}
