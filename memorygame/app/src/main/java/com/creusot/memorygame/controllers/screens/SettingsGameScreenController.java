package com.creusot.memorygame.controllers.screens;

import android.support.annotation.NonNull;

import com.creusot.memorygame.screens.SettingsGameScreenFragment;

/**
 * Created by Cédric Creusot on 19/12/15.
 */
public class SettingsGameScreenController {

    public static final int NUMBER_OF_GRID = 2;
    public static final int MULTIPLY_VALUE = 2;
    private static final int STARTING_GRID_SIZE = 2;

    private int gridSize;
    private final SettingsGameScreenFragment settingsGameScreenFragment;

    public SettingsGameScreenController(@NonNull SettingsGameScreenFragment settingsGameScreenFragment) {
        this.settingsGameScreenFragment = settingsGameScreenFragment;
    }

    public void initialize() {
        gridSize = STARTING_GRID_SIZE;

        settingsGameScreenFragment.setSeekbarSizeMax(NUMBER_OF_GRID);
        settingsGameScreenFragment.setSizeIndicatorGridSizeText(gridSize);
        settingsGameScreenFragment.setGridPreviewSize(gridSize, gridSize);
    }

    public int getGridSize() {
        return gridSize;
    }

    public void updateGridSize(int seekbarProgress) {
        gridSize = (seekbarProgress + 1) * MULTIPLY_VALUE;
        if (seekbarProgress == 0) {
            gridSize = STARTING_GRID_SIZE;
        }

        settingsGameScreenFragment.setSizeIndicatorGridSizeText(gridSize);
        settingsGameScreenFragment.setGridPreviewSize(gridSize, gridSize);
    }
}
