package com.creusot.memorygame.screens.interfaces;

/**
 * Created by Cédric Creusot on 19/12/15.
 */
public interface BaseScreenFragment {
    String getTransactionTag();
}
