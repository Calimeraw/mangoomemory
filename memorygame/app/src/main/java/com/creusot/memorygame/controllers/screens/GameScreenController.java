package com.creusot.memorygame.controllers.screens;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;

import com.creusot.memorygame.R;
import com.creusot.memorygame.screens.GameScreenFragment;
import com.creusot.memorygame.views.GameCardView;

/**
 * Created by Cédric Creusot on 20/12/15
 */
public class GameScreenController {

    public static final int GAME_STATE_DO_NOTHING = 0;
    public static final int GAME_STATE_WIN = 1;
    public static final int GAME_STATE_PAIR_EQUAL = 2;
    public static final int GAME_STATE_PAIR_NOT_EQUAL = 3;

    private static final int DEFAULT_GRID_SIZE = 2;
    private static final int[] ICON_RESOURCES = {
            R.drawable.icon0,
            R.drawable.icon1,
            R.drawable.icon2,
            R.drawable.icon3,
            R.drawable.icon4,
            R.drawable.icon5,
            R.drawable.icon6,
            R.drawable.icon7,
            R.drawable.icon8,
            R.drawable.icon9,
            R.drawable.icon10,
            R.drawable.icon11,
            R.drawable.icon12,
            R.drawable.icon13,
            R.drawable.icon14,
            R.drawable.icon15,
            R.drawable.icon16,
            R.drawable.icon17,
    };

    private GameScreenFragment gameScreenFragment;
    private int gridSize = DEFAULT_GRID_SIZE;
    private int countPair;
    private long lastTime;
    private long elaspedTime;
    private int gameState;

    public GameScreenController(GameScreenFragment gameScreenFragment) {
        this.gameScreenFragment = gameScreenFragment;
    }

    public void initialize() {
        Bundle argument = gameScreenFragment.getArguments();
        if (argument != null) {
            gridSize = argument.getInt(GameScreenFragment.GRID_SIZE, DEFAULT_GRID_SIZE);
        }
        countPair = (gridSize * gridSize) / 2;
        gameScreenFragment.setGridSizeGameGridLayout(gridSize);
        gameScreenFragment.setGameGridLayoutData(generateGrid());

        lastTime = System.currentTimeMillis();
        gameScreenFragment.setTime(getElapsedTime());
    }

    @VisibleForTesting
    public int[][] generateGrid() {
        int[] countPerResources = new int[countPair];
        int[][] grid = new int[gridSize][gridSize];

        while (!isAllResourcesSet(countPerResources)) {
            for (int x = 0; x < gridSize; x++) {
                for (int y = 0; y < gridSize; y++) {
                    int selectedResourceIndex = (int) (Math.random() * countPerResources.length);
                    int selectedResource = ICON_RESOURCES[selectedResourceIndex];
                    if (countPerResources[selectedResourceIndex] < 2) {
                        if (grid[x][y] == 0) {
                            countPerResources[selectedResourceIndex]++;
                            grid[x][y] = selectedResource;
                        }
                    }
                }
            }
        }

        return grid;
    }

    @VisibleForTesting
    public boolean isAllResourcesSet(int[] countPerResources) {
        int count = 0;
        for (int i = 0; i < countPerResources.length; i++) {
            if (countPerResources[i] == 2) {
                count++;
            }
        }
        return count == countPerResources.length;
    }

    public long getElapsedTime() {
        if (gameState != GAME_STATE_WIN) {
            long currentTime = System.currentTimeMillis();
            long timeToAdd = currentTime - lastTime;
            lastTime = currentTime;
            elaspedTime += timeToAdd;
        }
        return elaspedTime;
    }

    public void restartTimer() {
        lastTime = System.currentTimeMillis();
    }

    public int getGameState() {
        return gameState;
    }

    public int getGridSize() {
        return gridSize;
    }

    @VisibleForTesting
    public int getCountPair() {
        return countPair;
    }

    public int updateGameState(GameCardView gameCardView1, GameCardView gameCardView2) {
        gameState = GAME_STATE_DO_NOTHING;

        if (gameCardView1 != null && gameCardView2 != null) {
            boolean viewAreEquals = (gameCardView1.getImageFrontResourceId() == gameCardView2.getImageFrontResourceId());
            if (viewAreEquals) {
                countPair--;
            }
            gameState = viewAreEquals ? GAME_STATE_PAIR_EQUAL : GAME_STATE_PAIR_NOT_EQUAL;
        }

        if (countPair <= 0) {
            gameState = GAME_STATE_WIN;
        }
        return gameState;
    }
}
