package com.creusot.memorygame.views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ViewFlipper;

import com.creusot.memorygame.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Cédric Creusot on 19/12/15
 */
public class GameCardView extends LinearLayoutCompat {

    @Bind(R.id.gameCardViewFlipper)
    ViewFlipper gameCardViewFlipper;
    @Bind(R.id.frontGameCardView)
    AppCompatImageView frontGameCardView;

    private int imageResourceId;
    private boolean isFlipped;

    public GameCardView(Context context) {
        this(context, null);
    }

    public GameCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GameCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    private void initialize() {
        View view = inflate(getContext(), R.layout.view_game_card, this);
        ButterKnife.bind(this, view);
    }

    public void setImageFront(int resId) {
        imageResourceId = resId;
        frontGameCardView.setImageResource(resId);
    }

    public int getImageFrontResourceId() {
        return imageResourceId;
    }

    public boolean isFlipped() {
        return isFlipped;
    }

    public void flip() {
        if (!isFlipped) {
            gameCardViewFlipper.showNext();
        } else {
            gameCardViewFlipper.showPrevious();
        }
        isFlipped = !isFlipped;
    }
}
