package com.creusot.memorygame.adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.creusot.memorygame.R;
import com.creusot.memorygame.controllers.screens.SettingsGameScreenController;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cédric Creusot on 20/12/15
 */
public class ScoreListAdapter extends BaseAdapter {

    public static final String LEVEL_FORMAT = "%d";
    private LayoutInflater layoutInflater;
    private List<String> scoreList;

    public ScoreListAdapter(LayoutInflater layoutInflater, Context context) {
        this.layoutInflater = layoutInflater;
        scoreList = new ArrayList<>();
        for (int i = 1; i <= getCount(); i++) {
            int gridSize = i * SettingsGameScreenController.MULTIPLY_VALUE;
            long timeElapsed = Prefs.getLong(String.format(LEVEL_FORMAT, gridSize), 0);
            String text = timeElapsed == 0 ? context.getString(R.string.scoreScreen_empty_text) : DateUtils.formatElapsedTime(timeElapsed);
            scoreList.add(context.getString(R.string.settingsGame_size_indicator, gridSize, gridSize) + " " + context.getString(R.string.gameScreen_timer_text, text));
        }
    }

    @Override
    public int getCount() {
        return SettingsGameScreenController.NUMBER_OF_GRID + 1;
    }

    @Override
    public Object getItem(int position) {
        return scoreList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = layoutInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(scoreList.get(position));
        return view;
    }
}
