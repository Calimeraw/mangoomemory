package com.creusot.memorygame.views;

import android.content.Context;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;

/**
 * Created by Cédric Creusot on 19/12/15
 */
public class GameGridLayout extends GridLayout {

    public GameGridLayout(Context context) {
        super(context);
    }

    public GameGridLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameGridLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * this method will populate the view.
     */
    public void populate(int[][] gridIcons, OnClickListener onClickListener) {
        final int width = getMeasuredWidth() / getColumnCount();
        final int height = getMeasuredHeight() / getRowCount();

        for (int row = 0; row < getRowCount(); row++) {
            for (int column = 0; column < getColumnCount(); column++) {
                GameCardView gameCardView = new GameCardView(getContext());
                gameCardView.setImageFront(gridIcons[row][column]);
                gameCardView.setOnClickListener(onClickListener);

                LayoutParams params = new LayoutParams(spec(row, CENTER), spec(column, CENTER));
                params.width = width;
                params.height = height;
                gameCardView.setLayoutParams(params);
                addView(gameCardView);
            }
        }
    }
}
