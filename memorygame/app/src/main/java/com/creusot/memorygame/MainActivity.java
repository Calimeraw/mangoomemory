package com.creusot.memorygame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.creusot.memorygame.screens.MainScreenFragment;
import com.creusot.memorygame.utils.ScreenFragmentUtils;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ScreenFragmentUtils.addScreenFragment(this, new MainScreenFragment());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
