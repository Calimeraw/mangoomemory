package com.creusot.memorygame.utils;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.creusot.memorygame.R;
import com.creusot.memorygame.screens.interfaces.BaseScreenFragment;

/**
 * Created by Cédric Creusot on 19/12/15.
 */
public class ScreenFragmentUtils {

    public static void addScreenFragment(@NonNull FragmentActivity fragmentActivity, @NonNull BaseScreenFragment baseScreenFragment) {
        if (baseScreenFragment instanceof Fragment) {
            FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .add(R.id.containerFrameLayout, (Fragment) baseScreenFragment, baseScreenFragment.getTransactionTag())
                    .commit();
        }
    }

    public static void replaceScreenFragment(@NonNull FragmentActivity fragmentActivity, BaseScreenFragment baseScreenFragment, boolean addToBackStack) {
        if (baseScreenFragment instanceof Fragment) {
            FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.replace(R.id.containerFrameLayout, (Fragment) baseScreenFragment, baseScreenFragment.getTransactionTag())
                    .commit();
        }
    }
}
