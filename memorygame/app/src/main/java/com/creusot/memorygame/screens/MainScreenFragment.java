package com.creusot.memorygame.screens;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.creusot.memorygame.R;
import com.creusot.memorygame.screens.interfaces.BaseScreenFragment;
import com.creusot.memorygame.utils.ScreenFragmentUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Cédric Creusot on 19/12/15.
 */
public class MainScreenFragment extends Fragment implements BaseScreenFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_screen, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.playAppCompatButton)
    public void onPlayAppCompatButtonClick(View view) {
        ScreenFragmentUtils.replaceScreenFragment(getActivity(), new SettingsGameScreenFragment(), true);
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.scoreAppCompatButton)
    public void onScoreAppCompatButtonClick(View view) {
        ScreenFragmentUtils.replaceScreenFragment(getActivity(), new ScoreScreenFragment(), true);
    }

    @Override
    public String getTransactionTag() {
        return MainScreenFragment.class.getName();
    }
}
