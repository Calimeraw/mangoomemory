package com.creusot.memorygame.screens;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.creusot.memorygame.R;
import com.creusot.memorygame.controllers.screens.SettingsGameScreenController;
import com.creusot.memorygame.screens.interfaces.BaseScreenFragment;
import com.creusot.memorygame.utils.ScreenFragmentUtils;
import com.creusot.memorygame.views.SettingsPreviewGridLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Cédric Creusot on 19/12/15.
 */
public class SettingsGameScreenFragment extends Fragment implements BaseScreenFragment {

    @Bind(R.id.settingsPreviewGridLayout)
    SettingsPreviewGridLayout settingsPreviewGridLayout;
    @Bind(R.id.gridSizeAppCompatSeekBar)
    AppCompatSeekBar gridSizeAppCompatSeekBar;
    @Bind(R.id.sizeIndicatorAppCompatTextView)
    AppCompatTextView sizeIndicatorAppCompatTextView;

    private final SettingsGameScreenController settingsGameScreenController = new SettingsGameScreenController(this);
    private final Handler handler = new Handler();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings_game_screen, container, false);
        ButterKnife.bind(this, view);

        initializeController();
        initializeListeners();
        return view;
    }

    private void initializeController() {
        settingsGameScreenController.initialize();
    }

    private void initializeListeners() {
        gridSizeAppCompatSeekBar.setOnSeekBarChangeListener(new GridSizeSeekBarChangeListener());
    }

    public void setSizeIndicatorGridSizeText(int gridSize) {
        sizeIndicatorAppCompatTextView.setText(getString(R.string.settingsGame_size_indicator, gridSize, gridSize));
    }

    public void setSeekbarSizeMax(int sizeMax) {
        gridSizeAppCompatSeekBar.setMax(sizeMax);
    }

    public void setGridPreviewSize(int column, int row) {
        settingsPreviewGridLayout.removeAllViews();
        settingsPreviewGridLayout.setSize(column, row);

        // Doing this we populate after that the fragment and the view have finished to inflate.
        handler.post(new PopulateSettingsPreviewGridLayoutRunnable());
    }

    @Override
    public String getTransactionTag() {
        return SettingsGameScreenFragment.class.getName();
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.startGameAppCompatButton)
    public void onStartGameAppCompatButtonClick(View view) {
        ScreenFragmentUtils.replaceScreenFragment(getActivity(),
                GameScreenFragment.newInstance(settingsGameScreenController.getGridSize()),
                true);
    }

    private class GridSizeSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            settingsGameScreenController.updateGridSize(progress);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // do nothing
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // do nothing
        }
    }

    private class PopulateSettingsPreviewGridLayoutRunnable implements Runnable {
        @Override
        public void run() {
            settingsPreviewGridLayout.populate();
        }
    }
}
