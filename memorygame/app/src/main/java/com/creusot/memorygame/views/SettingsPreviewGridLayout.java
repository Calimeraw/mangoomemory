package com.creusot.memorygame.views;

import android.content.Context;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.view.View;

import com.creusot.memorygame.R;

/**
 * Created by Cédric Creusot on 19/12/15
 */
public class SettingsPreviewGridLayout extends GridLayout {

    public SettingsPreviewGridLayout(Context context) {
        super(context);
    }

    public SettingsPreviewGridLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SettingsPreviewGridLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setSize(int column, int row) {
        setColumnCount(column);
        setRowCount(row);
    }

    /**
     * this method will populate the view.
     */
    public void populate() {
        final int width = getMeasuredWidth() / getColumnCount();
        final int height = getMeasuredHeight() / getRowCount();

        for (int row = 0; row < getRowCount(); row++) {
            for (int column = 0; column < getColumnCount(); column++) {
                View view = new View(getContext());
                view.setBackgroundResource(R.drawable.back_background_card);

                LayoutParams params = new LayoutParams(spec(row, CENTER), spec(column, CENTER));
                params.width = width;
                params.height = height;
                view.setLayoutParams(params);
                addView(view);
            }
        }
    }
}
