package com.creusot.memorygame.screens;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.creusot.memorygame.R;
import com.creusot.memorygame.controllers.screens.GameScreenController;
import com.creusot.memorygame.screens.interfaces.BaseScreenFragment;
import com.creusot.memorygame.views.GameCardView;
import com.creusot.memorygame.views.GameGridLayout;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Cédric Creusot on 19/12/15.
 */
public class GameScreenFragment extends Fragment implements BaseScreenFragment {

    public static final String GRID_SIZE = "grid_size";
    public static final int DELAY_MILLIS_TIMER = 100;
    public static final int DELAY_MILLIS_REVERT = 1000;
    public static final String LEVEL_FORMAT = "%d";

    @Bind(R.id.timerAppCompatTextView)
    AppCompatTextView timerAppCompatTextView;
    @Bind(R.id.gameGridLayout)
    GameGridLayout gameGridLayout;


    private GameCardView firstGameCardViewSelected;
    private Handler handler = new Handler();
    private GameScreenController gameScreenController = new GameScreenController(this);

    private Runnable updateTimerRunnable;
    private Runnable revertCardStateRunnable;

    public static GameScreenFragment newInstance(int gridSize) {
        GameScreenFragment gameScreenFragment = new GameScreenFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(GRID_SIZE, gridSize);
        gameScreenFragment.setArguments(arguments);
        return gameScreenFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game_screen, container, false);
        ButterKnife.bind(this, view);
        initializeController();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        gameScreenController.restartTimer();
        handler.postDelayed(updateTimerRunnable, DELAY_MILLIS_TIMER);
        handler.postDelayed(revertCardStateRunnable, DELAY_MILLIS_REVERT);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(updateTimerRunnable);
        handler.removeCallbacks(revertCardStateRunnable);
    }

    private void initializeController() {
        gameScreenController.initialize();
    }

    public void setGridSizeGameGridLayout(int gridSize) {
        gameGridLayout.setColumnCount(gridSize);
        gameGridLayout.setRowCount(gridSize);
    }

    public void setGameGridLayoutData(final int[][] gridIcons) {
        // Doing this we populate after that the fragment and the view have finished to inflate.
        handler.post(new PopulateGameGridLayoutRunnable(gridIcons));
    }

    public void setTime(long time) {
        if (gameScreenController.getGameState() != GameScreenController.GAME_STATE_WIN) {
            if (getActivity() != null) {
                timerAppCompatTextView.setText(getString(R.string.gameScreen_timer_text, DateUtils.formatElapsedTime(time)));
            }

            updateTimerRunnable = new UpdateTimerRunnable();
            handler.postDelayed(updateTimerRunnable, DELAY_MILLIS_TIMER);
        }
    }

    @Override
    public String getTransactionTag() {
        return GameScreenFragment.class.getName();
    }

    private class PopulateGameGridLayoutRunnable implements Runnable {
        private final int[][] gridIcons;

        public PopulateGameGridLayoutRunnable(int[][] gridIcons) {
            this.gridIcons = gridIcons;
        }

        @Override
        public void run() {
            gameGridLayout.populate(gridIcons, new GameCardClickListener());
        }

    }

    private class GameCardClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View view) {
            if (view instanceof GameCardView && gameGridLayout.isEnabled()) {
                ((GameCardView) view).flip();

                int gameState = gameScreenController.updateGameState(firstGameCardViewSelected, (GameCardView) view);
                if (firstGameCardViewSelected == null) {
                    firstGameCardViewSelected = (GameCardView) view;
                    firstGameCardViewSelected.setEnabled(false);
                } else if (gameState == GameScreenController.GAME_STATE_PAIR_EQUAL) {
                    view.setEnabled(false);
                    firstGameCardViewSelected = null;
                } else if (gameState == GameScreenController.GAME_STATE_PAIR_NOT_EQUAL) {
                    gameGridLayout.setEnabled(false);

                    revertCardStateRunnable = new RevertCardStateRunnable(view);
                    handler.postDelayed(revertCardStateRunnable, DELAY_MILLIS_REVERT);
                } else if (gameState == GameScreenController.GAME_STATE_WIN) {
                    AlertDialog.Builder alterDialogBuilder = new AlertDialog.Builder(getActivity());
                    alterDialogBuilder.setTitle(R.string.gameScreen_alertDialog_title)
                            .setCancelable(false)
                            .setPositiveButton(R.string.gameScreen_alertDialog_ok, new WinAlertDialogOnPositiveButtonClickListener()).show();
                    handler.removeCallbacks(updateTimerRunnable);
                }
            }
        }

        private class WinAlertDialogOnPositiveButtonClickListener implements DialogInterface.OnClickListener {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                String level = String.format(LEVEL_FORMAT, gameScreenController.getGridSize());
                long lastElasped = Prefs.getLong(level, 0);
                if (lastElasped == 0 || lastElasped > gameScreenController.getElapsedTime()) {
                    Prefs.putLong(level, gameScreenController.getElapsedTime());
                }
                getActivity().onBackPressed();
            }
        }
    }

    private class RevertCardStateRunnable implements Runnable {
            private final View view;

            public RevertCardStateRunnable(View view) {
                this.view = view;
            }

            @Override
            public void run() {
                gameGridLayout.setEnabled(true);
                if (firstGameCardViewSelected != null) {
                    firstGameCardViewSelected.setEnabled(true);
                    firstGameCardViewSelected.flip();
                }
                if (view != null) {
                    view.setEnabled(true);
                    ((GameCardView) view).flip();
                }
                firstGameCardViewSelected = null;
            }
        }


    private class UpdateTimerRunnable implements Runnable {
        @Override
        public void run() {
            setTime(gameScreenController.getElapsedTime());
        }
    }
}
